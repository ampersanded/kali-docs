---
title: Contribute to Kali
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## Kali Linux Mirror Contributions

Mirrors are always good to have. The more we have, and the faster they are, the better. For more information about becoming an official Kali Linux mirror, check out our [Mirror Policy Page](/docs/community/kali-linux-mirrors/).
