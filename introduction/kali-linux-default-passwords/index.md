---
title: Kali Linux Default Passwords
description:
icon:
date: 2019-10-26
type: post
weight: 40
author: ["g0tmi1k",]
tags: ["needs-review",]
keywords: [""]
og_description:
---

## Kali Linux Default root Password is toor

### Default root Password

During installation, Kali Linux allows users to configure a password for the _root_ user. However, should you decide to boot the live image instead, the i386, amd64, VMWare and ARM images are configured with the **default root password - "toor"**, without the quotes.

- beef
- OpenVAS
- MySQL
