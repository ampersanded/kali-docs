---
title: NetHunter Home Screen
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter Home Screen

The NetHunter Home screen provides a common place to see some useful, frequently-used information about your device, including both external and internal IP addresses, as well as the availability of your HID interfaces.

![](/docs/nethunter/images/nethunter-home.png)
