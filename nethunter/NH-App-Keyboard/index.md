---
title: NetHunter Application - Keyboard
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter Application - Keyboard
This application allows to open up one of several kinds of terminals - a chrooted Kali terminal, an Android terminal, and a root Android terminal.
