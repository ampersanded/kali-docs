---
title: NetHunter Application - Csploit
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter Application - Csploit
This application allows you to open up one of several kinds of terminals - a chrooted Kali terminal, a standard Android terminal, and a root Android terminal.
