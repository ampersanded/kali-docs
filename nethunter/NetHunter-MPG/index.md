---
title: NetHunter Metasploit Payload Generator
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter Metasploit Payload Generator

The [MSFvenom Payload Creator (MFSPC)](https://github.com/g0tmi1k/msfpc) was written by g0tmi1k to take the pain out of generating payloads using the Metasploit msfvenom utility. Simply select your payload, set its options, and generate your payload.

![](/docs/nethunter/images/nethunter-mpc.png)
