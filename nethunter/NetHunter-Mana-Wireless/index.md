---
title: NetHunter MANA Evil Access Point
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter MANA Evil Access Point

[The MANA Toolkit](https://github.com/sensepost/mana) is an evil access-point implementation by [SensePost](https://www.sensepost.com/) that performs rogue Wi-Fi AP and MitM attacks. The MitM logs get written to /var/lib/mana-toolkit/ in the Kali chroot.

The default MANA configuration should work as-is however, you can tweak any of the available settings to match your target environment such as the ssid, channel number, etc.

Once everything is configured to your satisfaction, tap the **Update** button to save the configuration.

![](/docs/nethunter/images/nethunter-mana.png)
