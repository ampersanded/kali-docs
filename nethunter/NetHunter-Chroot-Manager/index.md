---
title: NetHunter Chroot Manager
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter Chroot Manager

The NetHunter chroot manager allows you to download and install a Kali Linux ARMHF chroot (if one does not exist), as well as remove an existing chroot. In addition, one can install various Kali Linux metapackages as required.

![](/docs/nethunter/images/nethunter-chroot-01.png)

In general, the "kali-nethunter" metapackage contains everything needed to run NetHunter, so be sure to only add extra metapackages if they're really needed, especially if disk space is at a premium. To get a general idea of the disk space required for each of the metapackages, refer to the [Kali Linux Metapackages blog post](https://www.kali.org/news/kali-linux-metapackages/).

![](/docs/nethunter/images/nethunter-chroot-02.png)
