---
title: NetHunter DuckHunter Attacks
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter DuckHunter Attacks

The DuckHunter HID option allows you to quickly and easily convert [USB Rubber Ducky](https://github.com/hak5darren/USB-Rubber-Ducky) scripts into NetHunter HID Attacks format. You can choose an option from the **Example presets** menu or choose from a larger selection of preconfigured scripts at the [Duck Toolkit](https://ducktoolkit-411.rhcloud.com/Home.jsp) site.

![](/docs/nethunter/images/nethunter-duckhunter.png)
