---
title: NetHunter Application Update
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter Application Update

The Check App Update option will check online for any updates to the NetHunter Android application and allow you to update it on-the-fly to stay updated with the latest and greatest features.

If there aren't any updates available, you'll see a message displayed like the one in the screenshot below.

![](/docs/nethunter/images/nethunter-app-update.png)
