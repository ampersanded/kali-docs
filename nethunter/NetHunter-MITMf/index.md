---
title: NetHunter Man In The Middle Framework
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter Man In The Middle Framework

The Man in the Middle Framework was written by @byt3bl33d3r and provides you with convenient one-stop shopping for all of your MitM and network attack needs. It includes keylogging, cookie capture, ARP poisoning, injection, spoofing, and much, much more.

![](/docs/nethunter/images/nethunter-mitm-01.png)

![](/docs/nethunter/images/nethunter-mitm-02.png)

![](/docs/nethunter/images/nethunter-mitm-03.png)
