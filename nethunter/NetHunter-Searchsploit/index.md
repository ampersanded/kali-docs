---
title: NetHunter Exploit Database SearchSploit
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

## NetHunter Exploit Database SearchSploit

The SearchSploit pane allows you to easily search [The Exploit Database](https://www.exploit-db.com/) archive for entries based on criteria of your choosing. Once you've found an exploit of interest, you can choose to view it online or even edit it locally to tailor it to your particular target.

![](/docs/nethunter/images/nethunter-searchsploit.png)
